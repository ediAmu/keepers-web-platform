
$(function () {
	"use strict";

	const baseUrl = "https://stage-main-vod.keeperschildsafety.net";
	/*==================================================================
    [ Date Picker ]*/

	let inputBirthDate = $('input[name="BirthDate"]'); // birth date input variable

	// Init the Date Picker for child's date of birth
	inputBirthDate.daterangepicker({
		drops: 'up',
		singleDatePicker: true,
		showDropdowns: true,
		autoUpdateInput: false,
		minDate: moment().subtract(18, 'years'), // child's age must be lower than 18
		maxDate: moment().subtract(6, 'years') // child's age must be larger than 6
	});
	// listener to date choise on date picker - update the birth date field with the date chosen
	inputBirthDate.on('apply.daterangepicker', function (ev, picker) {
		$(this).val(picker.startDate.format('DD/MM/YYYY'));
	});
	// listener to canseling on date picker - update the birth date field to be empty
	inputBirthDate.on('cancel.daterangepicker', function () {
		$(this).val('');
	});

    /*==================================================================
    [ Validate ]*/


	var input = $('.validate-input .input100');	// input - array of all input fields (4)
	var storage = chrome.storage.local;

	// clear the chrome.storage.local storage
	// storage.clear(function () {
	// 	var error = chrome.runtime.lastError;
	// 	if (error) {
	// 		console.error(error);
	// 	}
	// });


	// if id, auth and name are saved to storage show registered screen
	storage.get(['id', 'authToken', 'name'], function (result) {
		if (result.id && result.authToken && result.name) {
			$("body").css({ "background-size": "contain", "background-position": "center" });
			registered(result.name);
		}
	});

	$('.validate-form').on('submit', function (e) {
		// var id = 610;
		// var authToken = "38b55370-21e9-419c-9404-2619ffebda09"
		// // debugger;
		// storage.set({ id: id }, function () {
		// 	console.log('id is set to ' + id);
		// });

		// storage.set({ authToken: authToken }, function () {
		// 	console.log('authToken is set to ' + authToken);
		// });

		// storage.set({ name: "PC Test 4" }, function () {
		// 	console.log('authToken is set to ' + "Edi's PC");
		// });

		var check = true;

		// jQuery for each loop check the inputs
		$.each(input, function (i, inp) {
			if (validate(input[i]) == false) {
				showValidate(input[i]);
				check = false;
				console.log("false in loop " + i);
			}
		});

		// check if there is a problem in the input fields
		if (check) {
			logIn(input);
		}
		e.preventDefault();
	});


	$('.validate-form .input100').each(function () {
		$(this).focus(function () {
			hideValidate(this);
		});
	});

	function validate(input) {
		// If it's email field check if it's valid
		if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
			if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
				return false;
			}
		}
		// If not email field check if the field is empty
		else {
			if ($(input).val().trim() == '') {
				return false;
			}
		}
	}

	function showValidate(input) {
		var thisAlert = $(input).parent();

		$(thisAlert).addClass('alert-validate');
	}

	function hideValidate(input) {
		var thisAlert = $(input).parent();

		$(thisAlert).removeClass('alert-validate');
	}

	function logIn(input) {

		var body = {
			"parent": {
				'email': input[0].value,
				'password': input[1].value,
				'isTest': true
			},
			"child": {
				'name': input[2].value,
				'dateOfBirth': moment(new Date(input[3].value)),
				'msisdn': Math.floor(1000 + Math.random() * 9000)
			}
		};

		// jQuery AJAX call to the server, creating new child
		$.ajax({
			type: 'post',
			url: baseUrl + '/children/createAndRegister',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			data: JSON.stringify(body),
			success: function (data) {
				// console.log("AJAX success with data : ");
				// console.log(data);

				storage.set({ id: data.id }, function () {
					console.log('id is set to ' + data.id);
				});

				storage.set({ authToken: data.authToken }, function () {
					console.log('authToken is set to ' + data.authToken);
				});

				storage.set({ name: input[2].value }, function () {
					console.log('authToken is set to ' + input[2].value);
				});

				// change the screen from form (registration) to registered
				registered(input[2].value);
			},
			error: function (error) {
				console.log("AJAX error: ");
				console.log(eval(error));
			}
		});
	}

	function registered(name) {
		var allPage = $('.login100-form');
		var title = $('.login100-form-title');
		var hid = $(' .wrap-input100');
		var bt = $('.login100-form-btn');

		allPage.fadeOut(1).fadeIn('slow');

		title[0].innerText = 'Your child "' + name + '" is successfully registered !';
		$("body").css({ "background-size": "contain", "background-position": "center" });
		input.hide();
		hid.hide();
		bt.hide();
	}
});
