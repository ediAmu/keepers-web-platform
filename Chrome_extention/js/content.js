'use strict';

// window.location.hostname - only the site address
// window.location.href - full address


$(function () { // jqery function that whaits for the page to load and only than runs the code

	// code that runs in facebook, in the futer can run different codes for different web-sites
	if (window.location.hostname === "facebook.com" || window.location.hostname === "www.facebook.com") {
		var storage = chrome.storage.local;		//shortcut to extension storage memory

		// get id and auth from storage for the API requests
		storage.get(['id', 'authToken', 'name'], function (result) {
			if (result.id && result.authToken) {
				var userID = result.id;
				var userAuth = result.authToken;
				var userName = result.name;
				var ownerName = $('._1vp5')[0].innerText;
				var obj = {};

				var deleteStorage = true; // boolean variable - true to clear storage on load

				// function that clear the storage (from 'sent dates)
				if (deleteStorage) {
					storage.clear(function () {
						var error = chrome.runtime.lastError;
						if (error) {
							console.error(error);
						}
						storage.set({ id: userID }, function () {
							// console.log('id is set to ' + userID);
						});

						storage.set({ authToken: userAuth }, function () {
							// console.log('authToken is set to ' + userAuth);
						});

						storage.set({ name: userName }, function () {
							// console.log('name is set to ' + userName);
						});
					});
				}

				// get all items from storage
				storage.get(null, function (items) {
					var allKeys = Object.keys(items);
					obj = items;
				});

				// create the observer and the callback function that will be activated when the tree (from this node and down) will be changed
				var mutationObserver = new MutationObserver(function (mutations) {
					var b = $('._59v1'); // class element that contains all chat tabs, indicates if chats are open or not
					var c = $('._5qib'); // array of all open chat tabs
					var d = $('._1ogo'); // array of all chat names
					var e = document.querySelector('._4tdt'); // class of all the messages
					var chat = new Array();
					var isGroup = false;
					var timeSent = new Array();
					var tempMinTimeSent = null;
					var newMessageWritten = false;
					var minAgoDate = new Date().getTime() - 60000; // (60000 ms = 1 min)

					mutations.forEach(function (mutation) {

						// d[0].innerText - name of the person of the last opend chat
						// d.length - amount of open chats

						if (mutation.addedNodes.length && b.length && c.length && d.length && d[0].innerText) {
							const target = mutation.target;
							const jqTar = $(target);
							var chatDivs = jqTar.find('div._4tdt');
							if (chatDivs.length) {
								var dateMilli;
								var title;

								//find the chat title and 'timeSent'
								let closestAncestor = chatDivs[0].closest(".fbNubFlyoutInner");
								let jqAncestor = $(closestAncestor)
								title = jqAncestor.find("span._1ogo")[0].innerText;
								if (title.includes(',') || title.includes('and') || title.includes('others'))
									isGroup = true;
								if (obj[title])
									timeSent = obj[title];

								chatDivs.each(function (index) {
									var singleMessage; // object that contains all the info of a single message 
									var textInMessage = "";

									// some times two different chats mix up in chatDivs variable
									// check if the chat of this message is the same as the first message

									let thisMessageClosestAncestor = $(this).closest(".fbNubFlyoutInner");
									let thisMessageJqAncestor = $(thisMessageClosestAncestor)
									let thisMessageTitle = thisMessageJqAncestor.find("span._1ogo")[0].innerText;

									if (title.localeCompare(thisMessageTitle))
										return true; // goes to next loop of 'each' (break the loop on false)

									//if message sent from the owner
									if ($(this).hasClass("_ua0")) {
										let timeStemp = $(this).find('[data-tooltip-content]')[0].getAttribute('data-tooltip-content');
										let timeArray = timeStemp.split(new RegExp([' ', ','].join('|'), 'g'));
										dateMilli = getUnixDate(timeArray);

										let textDivs = $(this).find("div._1aa6, title");
										textDivs.each(function () {
											textInMessage += $(this).text() + "\n";
										});

										if (textInMessage && dateMilli) {
											singleMessage = { text: textInMessage, watching_time: dateMilli, is_outgoing: true }

											// if this data sent to the server allready
											if (timeSent[0] && timeSent[1] && timeSent[0] <= dateMilli && timeSent[1] >= dateMilli) {
												// Do nothing (don't add the message to chat)
											}
											// if data wasn't sent and smaller than starting date
											else if (timeSent[0] && timeSent[0] > dateMilli) {
												chat.push(singleMessage);
												if (!tempMinTimeSent)
													tempMinTimeSent = dateMilli;
											}
											// if data wasn't sent and bigger than ending date
											else if (timeSent[1] && (timeSent[1] < dateMilli || minAgoDate < timeSent[1])) {
												chat.push(singleMessage);
												timeSent[1] = dateMilli;
												obj[title] = timeSent;
											}
											// if timeSent is empty - no data was sent to server for this conversation
											else {
												chat.push(singleMessage);
												timeSent[1] = timeSent[0] = dateMilli;
												obj[title] = timeSent;
											}

											// check the emount of messages in 'chat' array, if larger than 16 
											// or got the last message in the chat call send method 
											if (chat.length > 16 || index === chatDivs.length - 1) {
												var copyChat = chat.map(mess => Object.assign({}, mess));
												send(copyChat, isGroup, tempMinTimeSent, timeSent, title);
												chat = new Array();
											}
										}
									}

									// else if message reseived from someone
									else if ($(this).hasClass("_ua1")) {
										let timeStemp = $(this).find('[data-tooltip-content]')[0].getAttribute('data-tooltip-content');
										let timeArray = timeStemp.split(new RegExp([' ', ','].join('|'), 'g'));
										var senderName = timeArray[0] + " " + timeArray[1];
										dateMilli = getUnixDate(timeArray);

										if (isGroup)
											textInMessage += senderName + ": "

										let textDivs = $(this).find("div._1aa6, title");
										textDivs.each(function () {
											textInMessage += $(this).text() + "\n";
										});

										if (textInMessage !== "" && textInMessage !== (senderName + ": ") && dateMilli) {
											singleMessage = { text: textInMessage, watching_time: dateMilli, is_outgoing: false }

											// if this data sent to the server allready
											if (timeSent[0] && timeSent[1] && timeSent[0] <= dateMilli && timeSent[1] >= dateMilli) {
												// Do nothing (don't add the message to chat)
											}
											// if data wasn't sent and smaller than starting date
											else if (timeSent[0] && timeSent[0] > dateMilli) {
												chat.push(singleMessage);
												if (!tempMinTimeSent)
													tempMinTimeSent = dateMilli;
											}
											// if data wasn't sent and bigger than ending date
											else if (timeSent[1] && (timeSent[1] < dateMilli || minAgoDate < timeSent[1])) {
												chat.push(singleMessage);
												timeSent[1] = dateMilli;
												obj[title] = timeSent;
											}
											// if timeSent is empty - no data was sent to server for this conversation
											else {
												chat.push(singleMessage);
												timeSent[1] = timeSent[0] = dateMilli;
												obj[title] = timeSent;
											}

											// check the emount of messages in 'chat' array if larger than 16 call send method 
											if (chat.length > 16 || index === chatDivs.length - 1) {
												var copyChat = chat.map(mess => Object.assign({}, mess));
												send(copyChat, isGroup, tempMinTimeSent, timeSent, title);
												chat = new Array();
											}
										}
									}
								});
							}
						}
					});
				});

				addObserverIfNodeAvailable(); // get the node (from DOM tree) that will contain the chat bars (to observe)

				function addObserverIfNodeAvailable() {
					var a = document.querySelector('#ChatTabsPagelet'); // get the node (from DOM tree) that will contain the chat bars (to observe)
					if (!a) {
						//The node we need does not exist yet.
						//Wait 500ms and try again
						window.setTimeout(addObserverIfNodeAvailable, 500);
						return;
					}
					else {
						//	activate the observer over the node that contain the chat bars
						mutationObserver.observe(a, {
							childList: true,
							subtree: true
						});
						return;
					}
				}

				// function that receives string array of date and returns the date in unix milliseconds
				function getUnixDate(timeArray) {
					var day, month, year, time, dateMilli, DayOfWeek;
					var pm = false;

					timeArray.forEach(function (part) {
						if (part.includes(":")) {
							var pmLocation = part.toLowerCase().indexOf("pm");
							var amLocation = part.toLowerCase().indexOf("am");
							if (part.length > 5) {
								if (pmLocation > -1) {
									pm = true;
									time = part.slice(0, pmLocation) + part.slice(pmLocation + 2, part.length);
								}
								else if (amLocation > -1) {
									time = part.slice(0, amLocation) + part.slice(amLocation + 2, part.length);
								}
							}
							else
								time = part;
						}
						else if (part.length === 4 && /^\d+$/.test(part)) {
							year = part;
						}
						else if (part.length < 3 && /^\d+$/.test(part)) {
							day = part;
						}
						else if (part.toLowerCase() === "pm") {
							pm = true
						}
						else if (part.length > 2) {
							let tempMonth = getMonth(part);
							if (tempMonth)
								month = tempMonth;
							if (!month) {
								let tempDayOfWeek = getDayOfWeek(part);
								if (tempDayOfWeek)
									DayOfWeek = tempDayOfWeek;
							}
						}
					});

					if (year && month && day && time)
						dateMilli = new Date(year + '-' + month + '-' + day + ' ' + time).getTime();
					else if (time) {
						dateMilli = createDate(time);
						if (DayOfWeek) {
							let numOfDays = new Date(dateMilli).getDay() - DayOfWeek;
							dateMilli -= (numOfDays * 86400000)
						}
					}

					if (dateMilli && pm)
						dateMilli += 43200000;
					return dateMilli;
				}

				// function that converts the day of week name to number
				function getDayOfWeek(week) {
					switch (week) {
						case 'ראשון':
						case 'Sunday':
							return 0;

						case 'שני':
						case 'Monday':
							return 1;

						case 'שלישי':
						case 'Tuesday':
							return 2;

						case 'רביעי':
						case 'Wednesday':
							return 3;

						case 'חמישי':
						case 'Thursday':
							return 4;

						case 'שישי':
						case 'Friday':
							return 5;

						case 'שבת':
						case 'Saturday':
							return 6;
					}
				}

				// function that converts the month name to number
				function getMonth(month) {
					switch (month) {
						case 'בינואר':
						case 'January':
							return "01";

						case 'בפברואר':
						case 'February':
							return "02";

						case 'במרץ':
						case 'March':
							return "03";

						case 'באפריל':
						case 'April':
							return "04";

						case 'במאי':
						case 'May':
							return "05";

						case 'בינוי':
						case 'June':
							return "06";

						case 'ביולי':
						case 'July':
							return "07";

						case 'באוגוסט':
						case 'August':
							return "08";

						case 'בספטמבר':
						case 'September':
							return "09";

						case 'באוקטובר':
						case 'October':
							return "10";

						case 'בנובמבר':
						case 'November':
							return "11";

						case 'בדצמבר':
						case 'December':
							return "12";
					}
				}

				// function that creates full date from time element from today and return in milliseconds
				function createDate(time) {
					let day, month, year, currentDate;

					currentDate = new Date(); // current time 
					day = currentDate.getDate();
					month = currentDate.getMonth() + 1;
					year = currentDate.getFullYear();

					// create today date with the received time 
					return new Date(year + '-' + month + '-' + day + ' ' + time).getTime();
				}


				//function that arrange all the data for the AJAX call and send it (from background)
				function send(chat, isGroup, tempMinTimeSent, timeSent, title) {
					if (chat.length) { // chack if there is one message at list befor sending
						// if older messages are beeing sent update the start time
						if (tempMinTimeSent)
							timeSent[0] = tempMinTimeSent;

						// create the body of the AJAX call
						var body = {
							"appName": "FaceBook-PC",
							"is_group_chat": isGroup,
							"title": title,
							"timezone": Intl.DateTimeFormat().resolvedOptions().timeZone,
							"created_date": new Date(),
							"messages": chat
						};

						// send all info to background for sending to server 
						chrome.runtime.sendMessage({ body: body, userID: userID, userAuth: userAuth, timeSent: timeSent });
					}
				}

				//receive messages from background to see if sending succeeded or not 
				chrome.runtime.onMessage.addListener(function (response, sender, sendResponse) {
					// if succeeded update the 'obj' object and the storage
					if (response.success) {
						obj[response.data.title] = response.timeSent;
						storage.set(obj, function () {
						});
					}
					else {
						console.log('****** ERROR: ');
						console.log(eval(response.error));
					}
				});
			}
		});
	}
});
