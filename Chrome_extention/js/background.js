
'use strict';
console.log('background.js');

const baseUrl = "https://stage-main-vod.keeperschildsafety.net";

chrome.runtime.onMessage.addListener(function (response, sender, sendResponse) {

	$.ajax({
		type: 'put',
		url: baseUrl + '/conversations/' + response.userID + '/conversation',
		headers: {
			'auth': response.userAuth,
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		data: JSON.stringify(response.body),
		success: function (data) {
			console.log("AJAX success with data : ");
			console.log(data);
			sendResponse({ data: data, success: true });

			chrome.tabs.sendMessage(sender.tab.id, { success: true, data: data, timeSent: response.timeSent });
		},
		error: function (error) {
			console.log("AJAX error: ");
			console.log(eval(error));
			sendResponse({ error: error, success: false });
			chrome.tabs.sendMessage(sender.tab.id, { success: false, error: error });
		}
	});
});

