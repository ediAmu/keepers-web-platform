/* ---=== BottomPanel.js ===--- */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col, Image } from 'react-bootstrap';
import { GetBatteryLevel2 } from '../../serviceAPI';
import MyMap from '../MyMap';
import moment from 'moment';
import "../../styles/bottomPanel.css";
import "../../styles/box.css";
import b100 from '../../assets/battery/100%.png';
import b95 from '../../assets/battery/95%.png';
import b70 from '../../assets/battery/70%.png';
import b40 from '../../assets/battery/40%.png';
import b15 from '../../assets/battery/15%.png';
import offLine from '../../assets/battery/Keepers_wifi_is_off.png';

/*
    Bottom panel component, It has google maps component inside it and battery level indicator. 
*/
class BottomPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            batteryImages: [b100, b95, b70, b40, b15, offLine]
        }
        this.getBatteryImageIndex = this.getBatteryImageIndex.bind(this);
        this.getBattereLvl = this.getBattereLvl.bind(this);
    }

    // Ajax call get data from the server
    componentDidMount() {
        this.getBattereLvl()
    }

    componentDidUpdate() {
        if (this.props.update !== undefined && !this.props.update[4]) {
            this.getBattereLvl();
        }
        this.props.setUpdate(4);
    }

    // get the batteery percentage level from server
    getBattereLvl() {
        GetBatteryLevel2(this.props.childrens[this.props.currKid].id).then(res => {  // When respond package is with status 200
            this.setState({
                ...this.state,
                batteryLevel: (res.data.batteryPercentage === undefined || res.data.batteryPercentage === null) ? 0 : res.data.batteryPercentage,
                bIndex: this.getBatteryImageIndex(res.data)
            });
        }).catch(error => { // When respond package is with error status - 400 ...
            console.log(error);
        });
    }

    // returns the index of the images that describes the battery level
    getBatteryImageIndex(data) {
        var level = data.batteryPercentage;
        var time = moment(data.lastUpdated);
        if (moment().diff(time, 'minutes') > 60) {
            return 5;
        }
        else if (level > 95 && level <= 100) {
            return 0;
        } else if (level > 70 && level <= 95) {
            return 1;
        } else if (level > 50 && level <= 70) {
            return 2;
        } else if (level > 15 && level <= 50) {
            return 3;
        } else if (level < 15 && level > 0) {    // level <= 15
            return 4;
        } else {
            this.setState({
                ...this.state,
                batteryLevel: 0
            });
            return 4;
        }
    }

    render() {
        var bLvl = (this.state.bIndex !== 5) ? this.state.batteryLevel + "%" : "Your child's phone is disconected from the network";
        var font_size = (this.state.bIndex !== 5) ? '3vw' : '1vw';

        return (
            <div className="card" style={{ height: '29vh', padding: 10, marginTop: 10 }}>
                {/* google maps component */}
                <Col xs={10} style={{ padding: 0, height: 'auto' }}>
                    <MyMap />
                </Col>
                {/* battery level indicator */}
                <Col xs={2} style={{ padding: 0, height: '-webkit-fill-available' }}>
                    <div style={{ height: '-webkit-fill-available', marginLeft: 10, padding: 5, textAlign: "center" }}>
                        <p className="battery_usage"> {this.props.currLang.batteryState} </p>
                        <div className="bImage">
                            <Image src={this.state.batteryImages[this.state.bIndex]} width="60%" />
                        </div>
                        <div style={{ color: this.state.color, fontSize: font_size, marginTop: 10, fontWeight: "bold" }}>
                            {bLvl}
                        </div>
                    </div>
                </Col>
            </div>
        );
    }
}

// variable from redux
const mapStateToProps = (state) => {
    return {
        currKid: state.DashboardInfo.currTab,   // current child active tab
        childrens: state.DashboardInfo.childrens, // gets information of all childrens of the user through redux.
        currLang: state.DisplayLanguage.currLang,           // current language of the application
        update: state.DashboardInfo.updateData, // get update data, to see if need to update the map.
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUpdate: (val) => {
            dispatch({
                type: "SET_UPDATE",
                value: val
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BottomPanel);