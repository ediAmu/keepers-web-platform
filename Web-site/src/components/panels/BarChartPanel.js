/* ---=== BarChartPanel.js ===--- */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { GetUsageStatistics } from '../../serviceAPI';
import UsageTimeChart from '../charts/UsageTimeChart';

/*
    Bar chart panel get all the usage information needed.
    Getting and parsing all the usage data and sends it to usage time chart.
*/
class BarChartPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataAssigned: false,    // holds a boolean that indicate if the new data was assigned.
            labels: [],             // bar labels - used in the bar chart.
            dataSet: [],            // the data that we show on chart. 
            timeType: 'm',           // type of the data(in minutes or hours)
            totalScreenTime: null,
            dayAverageTime: null
        }
    }

    componentDidMount() {
        this.getUsageStatistics(this.props);
    }

    componentDidUpdate() {
        if (this.props.childIndex === this.props.currChild && this.props.update !== undefined && !this.props.update[1]) {
            this.props.setUpdate(1);
            this.getUsageStatistics();
        }
    }

    // gets the child usage data - how many hours he spent and in what.
    getUsageStatistics() {
        let startTime = this.props.startDate;
        let endTime = this.props.endDate;

        GetUsageStatistics(this.props.childrens[this.props.childIndex].id, startTime, endTime).then(res => {  // when respond package is with status 200
            this.buildChartData(res.data);// parse data
        }).catch((error) => { // when respond package is with error status - 400 ...
            console.log('error + error response:');
            console.log(error);
            console.error(error.response);
        });
    }

    setTotalScreenTime() {
        this.props.getTotalScreenTime(this.state.totalScreenTime, this.state.dayAverageTime);
    }

    // parsing the data recieved from keepers server.
    // gets the data and put the parsed data to state data set.
    buildChartData(data) {
        let type = 'm'; // default show type - in minutes.
        let apps = [];
        let totalTime = 0;
        let dayAverage = null;
        data.map((days) => {
            let appsArray = JSON.parse(days.usageData);

            appsArray.map((appData) => {
                let difference = moment.duration(appData.time);
                totalTime += difference;
                if (difference >= moment.duration(60, 'minutes'))
                    type = 'h'; // change type when there is an app with more then 60 minutes use.
                let appName = appData.name;
                if (apps[appName] === undefined)  // checks if app name already has a data set.
                    apps[appName] = 0;
                apps[appName] += appData.time;
                return null;
            });

            return null;
        });

        // arrange the data and app name in object.
        let tempData = Object.keys(apps).map((appName) => {
            let difference = apps[appName];
            return { appName: appName, count: difference };
        });

        let dataSet = [];

        // sorts the data by size and name.
        let labels = [].concat(tempData)
            .sort((a, b) => a.count > b.count ? -1 : 1)
            .map((item, i) => {
                dataSet.push(type === "m" ? moment.duration(item.count).asMinutes().toFixed() : moment.duration(item.count).asHours());
                return item.appName;
            });

        // No screen time
        if (totalTime === 0) {
            totalTime = null;
            dayAverage = null;
        }
        // Screen time in minuts
        else if (moment.duration(totalTime).asMinutes().toFixed() < 60) {
            if (this.props.range > 1)
                dayAverage = moment.duration(totalTime).asMinutes().toFixed() / (this.props.range + 1) + " Minutes";
            totalTime = moment.duration(totalTime).asMinutes().toFixed() + " Minutes";
        }
        // Screen time in hours
        else {
            if (this.props.range > 0) {
                dayAverage = totalTime / (this.props.range + 1);
                if (moment.duration(dayAverage).asMinutes().toFixed() < 60)
                    dayAverage = moment.duration(dayAverage).asMinutes().toFixed() + " Minutes";
                else
                    dayAverage = Number.parseInt(moment.duration(dayAverage).asHours(), 10) + ":" + ("0" + ((dayAverage / 60000) % 60).toFixed()).slice(-2) + " Housr";
            }

            totalTime = Number.parseInt(moment.duration(totalTime).asHours(), 10) + ":" + ("0" + ((totalTime / 60000) % 60).toFixed()).slice(-2) + " Housr";
        }

        this.setState({
            ...this.state,
            labels: labels.slice(0, 10),
            dataSet: dataSet.slice(0, 10),
            timeType: type,
            dataAssigned: true,
            totalScreenTime: totalTime,
            dayAverageTime: dayAverage
        }, () => {
            this.setTotalScreenTime(this);
        });
    }

    render() {
        return (
            this.state.dataAssigned && <UsageTimeChart style={{ height: 'inherit' }} labels={this.state.labels} data={this.state.dataSet} type={this.state.timeType} />); // shows the usage chart when there is data assigned. 
    }
}

// redux variables.
const mapStateToProps = (state) => {
    return {
        childrens: state.DashboardInfo.childrens,    // gets information of all childrens of the user through redux.
        startDate: moment(state.DashboardInfo.startDate),    // gets the start date the user looking for the information to start from throguh redux.
        endDate: moment(state.DashboardInfo.endDate),    // gets the end date the user looking for the information to end at throguh redux.
        range: state.DashboardInfo.datesRange,       // gets the range of the dates the user picked to see the data.
        update: state.DashboardInfo.updateData,      // gets the state of the component - if need to get the data.
        currChild: state.DashboardInfo.currTab       // gets the current children tab user at.
    };
};

// redux functions.
const mapDispatchToProps = (dispatch) => {
    return {
        setUpdate: (val) => {
            dispatch({
                type: "SET_UPDATE",
                value: val
            });
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BarChartPanel);

