/* ---=== TopPanel.js ===--- */

import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import MsgsPanel from './MsgsPanel';
import LineChartPanel from './LineChartPanel';
import BarChartPanel from './BarChartPanel';
/*
    Top panel component, inside this component we has line chart panel,
    bar chart panel and messages panel. As you can see they all get renders 
    inside this component and will shown in one screen.
*/
class TopPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalScreenTime: null,
            dayAverageTime: null
        }
    }

    receiveTotalScreenTime(totalTime, average) {
        this.setState({
            ...this.state,
            totalScreenTime: totalTime,
            dayAverageTime: average
        });
    }

    render() {
        return (
            <Row style={{ height: 54 + 'vh' }}>
                {/* Line chart panel and bar chart panel has their column */}
                <Col xs={8} md={9} style={{ padding: 0, height: 'inherit' }}>
                    {/* Row for line chart panel */}
                    <Row style={{ height: '46%' }}>
                        {this.props.range !== undefined && <LineChartPanel childIndex={this.props.childIndex} />}
                    </Row>
                    {/* Row for style between two chart panels */}
                    <Row style={{ height: '10%' }}>
                        <div>
                            <span className="date_title"><span aria-labelledby="clock icon" role="img" className="clock_icon">&#128340;</span>{" " + this.props.currLang.usage_time}</span>
                            {this.state.totalScreenTime && <span className="info"><b>{" " + this.props.currLang.total_usage_time + " "}</b>{this.state.totalScreenTime}</span>}
                            {this.state.dayAverageTime && <span className="info"><b>{" " + this.props.currLang.day_average_time + " "}</b>{this.state.dayAverageTime}</span>}
                            <hr className="line_hr" />
                        </div>
                    </Row>
                    {/* Row for bar chart panel */}
                    <Row style={{ height: '45%' }}>
                        {this.props.range !== undefined && <BarChartPanel childIndex={this.props.childIndex} getTotalScreenTime={this.receiveTotalScreenTime.bind(this)} />}
                    </Row>
                </Col>
                {/* Messages panel in new column */}
                <Col xs={4} md={3} lg={3} style={{ 'paddingRight': 0 + 'px', height: 'inherit' }}>
                    {this.props.range !== undefined && <MsgsPanel childIndex={this.props.childIndex} />}
                </Col>
            </Row>
        );
    }
}

// variable of redux
const mapStateToProps = (state) => {
    return {
        range: state.DashboardInfo.datesRange,      // the range of dates between startDate and endDate
        currLang: state.DisplayLanguage.currLang    // current language of the application
    };
};

export default connect(mapStateToProps)(TopPanel);