/* ---=== MyMap.js ===--- */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, withProps, withStateHandlers } from "recompose";
import { GetLocation } from '../serviceAPI';
import markerIcon from '../assets/map/marker.png';
import pointIcon from '../assets/map/point_to_map.png';
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Polyline } from "react-google-maps";
import moment from 'moment';

var language = window.navigator.userLanguage || window.navigator.language;

// Sets the google map component.
const MyMapComponent = compose(
	withProps({
		loadingElement: <div style={{ height: `100%` }} />,
		containerElement: <div style={{ height: `26vh` }} />,
		mapElement: <div style={{ height: `100%` }} />,
		googleMapURL: `https://maps.googleapis.com/maps/api/js?key=AIzaSyDQ5C6rccOVR2xulFhp2r_JuaD_OVWlXgw&language=${language}&region=${language}&v=3.exp&libraries=geometry,drawing,places`
	}),
	withScriptjs,
	withGoogleMap,
	withStateHandlers(() => ({
		isOpen: {},
		clicked: {},
		mapCenter: { lat: 31.766572, lng: 35.200025 },
		mapCenteredOneceBool: false,
		lastDateStemp: moment()
	}), {
			onToggleOpen: (state) => (index, center, fromD) => { // when opening the marker label in map.
				let newIsOpen = Object.assign({}, state.isOpen);
				let newMapCenter = Object.assign({}, state.mapCenter);
				let newDateStemp = Object.assign({}, moment(state.lastDateStemp).clone());
				newIsOpen[index] = !state.isOpen[index];

				if (moment(fromD).diff(state.lastDateStemp, "minutes") !== 0) {
					newDateStemp = Object.assign({}, moment(fromD).clone());
				}

				if (center && center.lng && center.lat) {
					newMapCenter = Object.assign({}, center);
				}
				return { isOpen: newIsOpen, mapCenter: newMapCenter, mapCenteredOneceBool: true, lastDateStemp: newDateStemp }
			},
			onClikedToggle: (state) => (index) => { // open the marker label.

				let newClicked = Object.assign({}, state.clicked);
				newClicked[index] = !state.clicked[index];
				return { clicked: newClicked }
			},
			onCloseToggle: (state) => (index) => { // when closing the marker label.

				let newIsOpen = Object.assign({}, state.isOpen);
				let newClicked = Object.assign({}, state.clicked);
				newIsOpen[index] = false;
				newClicked[index] = false;
				return { isOpen: newIsOpen, clicked: newClicked };
			},
		}),
)(props => {
	return (
		<GoogleMap
			zoom={props.defaultZoom}
			center={props.mapCenteredOneceBool || props.lastDateStemp.diff(props.fromD, "minutes") === 0 ? props.mapCenter : props.kidsLocation && props.kidsLocation[props.kidsLocation.length - 1] ? { lat: props.kidsLocation[props.kidsLocation.length - 1].latitude + 0.001, lng: props.kidsLocation[props.kidsLocation.length - 1].longitude } : { lat: 31.766572, lng: 35.200025 }}
			ref={props.onMapMounted}
		>
			{
				props.kidsLocation && props.kidsLocation.length > 0 &&
				<Marker
					position={props.kidsLocation && props.kidsLocation[props.kidsLocation.length - 1] ? { lat: props.kidsLocation[props.kidsLocation.length - 1].latitude, lng: props.kidsLocation[props.kidsLocation.length - 1].longitude } : {}}
					key={props.kidsLocation.length - 1}
					icon={markerIcon}

					onMouseOut={!props.clicked[props.kidsLocation.length - 1] ? props.onToggleOpen.bind(this, props.kidsLocation.length - 1, (props.kidsLocation && props.kidsLocation[props.kidsLocation.length - 1]) ? { lat: props.kidsLocation[props.kidsLocation.length - 1].latitude, lng: props.kidsLocation[props.kidsLocation.length - 1].longitude } : null, props.fromD) : null}
					onMouseOver={!props.clicked[props.kidsLocation.length - 1] ? props.onToggleOpen.bind(this, props.kidsLocation.length - 1, (props.kidsLocation && props.kidsLocation[props.kidsLocation.length - 1]) ? { lat: props.kidsLocation[props.kidsLocation.length - 1].latitude, lng: props.kidsLocation[props.kidsLocation.length - 1].longitude } : null, props.fromD) : null}
					onClick={props.onClikedToggle.bind(this, props.kidsLocation.length - 1)}
				>
					{
						props.isOpen[props.kidsLocation.length - 1] &&
						<InfoWindow onCloseClick={props.onCloseToggle.bind(this, props.kidsLocation.length - 1)}>
							<span>
								<div jstcache="33" className="poi-info-window gm-style">
									<div dir="rtl" jstcache="3" className="title full-width" jsan="0.dir,7.title,7.full-width">{props.childrens[props.currentTab].name}</div>
									<div>{moment(props.kidsLocation[props.kidsLocation.length - 1].dateCreated).format('LLL')}</div>
									<div>Address: {props.kidsLocation[props.kidsLocation.length - 1].address}</div>
								</div>
							</span>
						</InfoWindow>
					}
				</Marker>
			}
			{
				props.kidsLocation && props.kidsLocation.length > 0 &&
				props.kidsLocation.map((mark, index) => (
					index !== props.kidsLocation.length - 1 &&
					<Marker
						key={index}
						position={{ lat: mark.latitude, lng: mark.longitude }}
						icon={pointIcon}

						onMouseOut={!props.clicked[index] ? props.onToggleOpen.bind(this, index, { lat: mark.latitude, lng: mark.longitude }, props.fromD) : null}
						onMouseOver={!props.clicked[index] ? props.onToggleOpen.bind(this, index, { lat: mark.latitude, lng: mark.longitude }, props.fromD) : null}
						onClick={props.onClikedToggle.bind(this, index)}
					>
						{props.isOpen[index] &&
							<InfoWindow onCloseClick={props.onCloseToggle.bind(this, index)}>
								<span>
									<div jstcache="33" className="poi-info-window gm-style">
										<div dir="rtl" jstcache="3" className="title full-width" jsan="0.dir,7.title,7.full-width">{props.childrens[props.currentTab].name}</div>
										<div>{moment(props.kidsLocation[index].dateCreated).format('LLL')}</div>
										<div>Address: {props.kidsLocation[index].address}</div>
									</div>
								</span>
							</InfoWindow>}
					</Marker>
				))
			}
			{
				props.kidsLocation && props.kidsLocation.length > 0 &&
				<Polyline
					path={props.polyPath}
					geodesic={true}
					options={{
						strokeColor: "#77C8C0",
						strokeOpacity: 0.75,
						strokeWeight: 3
					}}
				/>
			}
		</GoogleMap>
	)
});



class MyMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
			lastChild: -1,
			timeStmep: moment()
		}
	}

	componentDidMount() {
		this.getCurrKidsLocations();
	}

	componentDidUpdate() {
		if (((this.props.update && !this.props.update[3]) || this.props.currentTab !== this.state.lastChild || !(this.state.fromD.isSame(this.props.startDate.startOf('day')))) && (moment().diff(this.state.timeStmep, 'seconds') > 2)) {
			this.setState({
				...this.state,
				timeStmep: moment()
			});
			setTimeout(this.getCurrKidsLocations(), 300);
		}
	}

	// send request to get kids locations.
	// gets the kid id and his index.
	getCurrKidsLocations() {
		this.props.setUpdate(3);
		var fromD, toD;

		// if not one day chosen or start date is after the begginig of today
		if (!this.props.isOneDay || this.props.startDate.diff(moment().startOf('day')) >= 0) {
			fromD = moment().startOf('day');
			toD = moment();
		}
		else {
			fromD = moment(this.props.startDate.clone());
			toD = moment(fromD.clone().add(1, 'day'));
		}

		// Ajax call get data from the server
		GetLocation(this.props.childrens[this.props.currentTab].id, fromD, toD).then(res => {  // when respond package is with status 200
			if (res.data.length !== 0) { // if there is data from server

				var showLoc = [];
				var polyLoc = [];
				showLoc[0] = res.data[0];
				polyLoc.push({ lat: res.data[0].latitude, lng: res.data[0].longitude });
				res.data.forEach(function (loc) {

					// insert the location to the array only if:
					//   1.the next location id is larger than the current 
					//   2. the distance between the locations is larger than 30 meters
					// otherwise pop the last location and push the new one instad
					if (!(loc.id > showLoc[showLoc.length - 1].id && dist(showLoc[showLoc.length - 1], loc) > 30)) {
						showLoc.pop();
						polyLoc.pop();
					}
					showLoc.push(loc);
					polyLoc.push({ lat: loc.latitude, lng: loc.longitude });
				});
				this.setState({
					...this.state,
					lastChild: this.props.currentTab,
					fromD,
					polyLoc
				});
			}
			if (showLoc && showLoc.length > 0) {
				this.props.setLocations(showLoc);
			}

		}).catch(error => { // When respond package is with error status - 400 ...
			console.log(error);
			console.error(error.response);
			console.log(error.response.data.code);
		});
	}

	render() {
		return (
			<MyMapComponent
				defaultZoom={this.props.defaultZoom}
				childrens={this.props.childrens}
				kidsLocation={this.props.kidsLocation}
				currentTab={this.props.currentTab}
				polyPath={this.state.polyLoc}
				fromD={this.state.fromD}
			/>
		);
	}
}

// function that receive two coordinates and return the distance between them in meters
function dist(cord1, cord2) {
	var R = 6371e3;  // Radius of the earth in meters
	var f1 = cord1.latitude.toRadians();
	var f2 = cord2.latitude.toRadians();
	var df = (cord2.latitude - cord1.latitude).toRadians();
	var dg = (cord2.longitude - cord1.longitude).toRadians();

	var a = Math.sin(df / 2) * Math.sin(df / 2) +
		Math.cos(f1) * Math.cos(f2) *
		Math.sin(dg / 2) * Math.sin(dg / 2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	return R * c;
}

// Extend Number object with methods to convert between degrees & radians
Number.prototype.toRadians = function () { return this * Math.PI / 180; };
Number.prototype.toDegrees = function () { return this * 180 / Math.PI; };

// variables used from redux.
const mapStateToProps = (state) => {
	return {
		childrens: state.DashboardInfo.childrens, // gets information of all childrens of the user through redux.
		kidsLocation: state.DashboardInfo.kidsLocation, // gets the kids location information from redux.
		currentTab: state.DashboardInfo.currTab, // get current tab kid.
		defaultZoom: state.DashboardInfo.defaultZoom, // get default zoom.
		update: state.DashboardInfo.updateData, // get update data, to see if need to update the map.
		startDate: moment(state.DashboardInfo.startDate),       // gets the start date the user looking for the information to start from throguh redux.
		endDate: moment(state.DashboardInfo.endDate),          // gets the range of the dates the user picked to see the data.
		isOneDay: state.DashboardInfo.isOneDay,         // gets the boolean to indicate if the data is for one day - for one day show statistics in hours, else show in days.
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		setUpdate: (val) => {
			dispatch({
				type: "SET_UPDATE",
				value: val
			});
		},
		setLocations: (val) => {
			dispatch({
				type: "SET_LOCATIONS",
				value: val
			});
		}
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(MyMap);
